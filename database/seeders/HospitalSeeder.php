<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Redis;

class HospitalSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $hospitals = [
            [
                "name" => "Cleveland Clinic",
                "coordinate" => [
                    "lat" => 53.5574, "lng" => -113.4967
                ]
            ],
            [
                "name" => "Toronto General Hospital",
                "coordinate" => [
                    "lat" => 53.5574, "lng" => -113.4967
                ]
            ],
            [
                "name" => "The Johns Hopkins Hospital",
                "coordinate" => [
                    "lat" => 53.5177, "lng" => -113.5313
                ]
            ],
            [
                "name" => "Mayo Clinic",
                "coordinate" => [
                    "lat" => 53.5205, "lng" => -113.5247
                ]
            ],
        ];

        Redis::pipeline(function ($pipe) use ($hospitals) {
            foreach ($hospitals as $hospital) {
                $pipe->geoadd("hospitals", $hospital["coordinate"]["lng"], $hospital["coordinate"]["lat"], $hospital["name"]);
            }
        });
    }
}
