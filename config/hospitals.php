<?php

use App\Responders\GreetResponder;
use App\Responders\HospitalResponder;

return [
    "responders" => [
        GreetResponder::class,
        HospitalResponder::class,
    ]
];
