<?php

namespace App\Responders;

use App\Constants\Keywords;
use App\Constants\Conversations;

class InvalidKeywordResponder extends Responder
{
    public static function shouldRespond($message, $longitude, $latitude): bool
    {
        return true;
    }

    public function respond(): string
    {
        return Conversations::INVALID_KEYWORD;
    }
}
