<?php

namespace App\Responders;

use Illuminate\Support\Facades\Redis;

class HospitalResponder extends Responder
{
    public static function shouldRespond($message, $longitude, $latitude): string
    {
        return $longitude && $latitude;
    }

    public function respond(): string
    {
        $nearestHospitalsInfo = Redis::georadius("hospitals", $this->longitude, $this->latitude, 5, "km", "WITHDIST");
        $nearestHospitalsList = "";
        $count = 1;
        foreach ($nearestHospitalsInfo as $hospitalInfo) {
            $nearestHospitalsList .= $count . " . " . "  "  . "*Name:*" . " " . $hospitalInfo[0] . " " . "," . " ";
            $nearestHospitalsList .= "*Distance from you:*" . " " . $hospitalInfo[1] . "meters" . "\n" . PHP_EOL;
            $count++;
        }

        return strlen($nearestHospitalsList) ? $nearestHospitalsList : "no nearby hospitals found.";
    }
}
