<?php

namespace App\Responders;

use App\Constants\Conversations;
use App\Constants\Keywords;

class GreetResponder extends Responder
{
    public static function shouldRespond(string|null $message, ?string $longitude, ?string $latitude): string
    {
        return $message === Keywords::GREET;
    }

    public function respond(): string
    {
        return Conversations::GREET;
    }
}
