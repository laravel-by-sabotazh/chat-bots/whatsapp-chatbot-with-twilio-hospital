<?php

namespace App\Responders;

use App\Contracts\ResponderContract;

abstract class Responder implements ResponderContract
{
    /** @var string|null */
    protected ?string $message;

    /** @var string|float|null */
    protected string|null|float $longitude;

    /** @var string|float|null */
    protected string|null|float $latitude;

    public function __construct(string $message, ?string $longitude, ?string $latitude)
    {
        $this->message = $message;
        $this->longitude = (float) $longitude;
        $this->latitude = (float) $latitude;
    }
}
