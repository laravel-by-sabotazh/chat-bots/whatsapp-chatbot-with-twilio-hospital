<?php

namespace App\Constants;

class Conversations
{
    const GREET = "HI Dear welcome, I am an automated system and will help find hospitals near you, please send me your current location.";

    const INVALID_KEYWORD = "Sorry I am an automated system and didn't understand your reply." . PHP_EOL .
    "Send hi to get started, or send me your current location to get a list of hospitals near you.";
}
