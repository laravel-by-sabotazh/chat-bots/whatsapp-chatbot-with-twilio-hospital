<?php

namespace App\Contracts;

interface ResponderContract
{
    /**
     * check whether to respond to a user message
     * @param string|null $message
     * @param string|null $longitude
     * @param string|null $latitude
     * @return bool|string
     */
    public static function shouldRespond(?string $message, ?string $longitude, ?string $latitude): bool|string;

    /**
     * respond to a user by returning a response
     * @return string
     */
    public function respond(): string;
}
