<?php

namespace App\Factories;

use Illuminate\Support\Facades\Redis;
use App\Responders\InvalidKeywordResponder;

class ResponderFactory
{
    /**
     * Responder
     * @var App\Contracts\ResponderContract;
     */
    protected $responder;

    /** @var string */
    protected $phoneNumber;

    /** @var string */
    protected $message;

    /** @var string */
    protected $longitude;

    /** @var string */
    protected $latitude;

    public function __construct(string $phonenumber, ?string $message, ?string $longitude, ?string $latitude)
    {
        $this->phoneNumber = $phonenumber;
        $this->message = $message;
        $this->longitude = $longitude;
        $this->latitude = $latitude;
        $this->responder = $this->resolveResponder($this->message, $this->longitude, $this->latitude);
    }

    /**
     * factory to create a responder.
     */
    public static function create()
    {
        $self =  new static(
            request()->input('From'),
            request()->input('Body'),
            request()->input('Longitude'),
            request()->input('Latitude')
        );

        return $self->responder;
    }

    /**
     * Trim and lower case the message
     * @param string|null $message
     * @return string|null
     */
    protected function normalizeMessage(?string $message): ?string
    {
        return trim(strtolower($message));
    }

    /**
     * Resolve responder
     * @param string|null $message
     * @param string|null $longitude
     * @param string|null $latitude
     * @return App\Contracts\Respondent
     */
    public function resolveResponder(?string $message, ?string $longitude, ?string $latitude)
    {
        $message =  $this->normalizeMessage($message);

        $responders = $this->getReponders();

        foreach ($responders as $responder) {
            if ($responder::shouldRespond($message, $longitude, $latitude)) {
                return new $responder($message, $longitude, $latitude);
            }
        }

        return new InvalidKeywordResponder($this->message, $this->longitude, $this->latitude);
    }

    /**
     * Get all available responders
     * @return array
     */
    public function getReponders(): array
    {
        return config('hospitals.responders');
    }
}
