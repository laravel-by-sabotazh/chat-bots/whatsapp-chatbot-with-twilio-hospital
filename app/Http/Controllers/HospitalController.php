<?php

namespace App\Http\Controllers;

use App\Factories\ResponderFactory;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Http\Response;
use Twilio\TwiML\MessagingResponse;

class HospitalController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param MessagingResponse $messageResponse
     * @return Application|ResponseFactory|Response
     */
    public function __invoke(MessagingResponse $messageResponse): Response|Application|ResponseFactory
    {
        $responder = ResponderFactory::create();
        $messageResponse->message($responder->respond());

        return response($messageResponse, 200)->header(
            'Content-Type',
            'text/xml'
        );
    }

    public function test()
    {
        return 'Ok';
    }
}
